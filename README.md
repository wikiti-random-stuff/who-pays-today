# Who pays?

## Summary

A simple application to know who must pay the breakfast today :smile:

## Installation

Clone this repo, install the dependencies with

````sh
bundle install
````

And execute the server with

````sh
rackup
````

## Authors ##

This project has been developed by:

| Avatar | Name | Nickname | Email |
| ------- | ------------- | --------- | ------------------ |
| ![](http://www.gravatar.com/avatar/2ae6d81e0605177ba9e17b19f54e6b6c.jpg?s=64)  | Daniel Herzog | Wikiti | [wikiti.doghound@gmail.com](mailto:wikiti.doghound@gmail.com) |
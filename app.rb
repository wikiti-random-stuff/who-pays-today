libdir = File.dirname(__FILE__)
$LOAD_PATH.unshift(libdir) unless $LOAD_PATH.include?(libdir)

# Dependencies
require 'sinatra'
require 'data_mapper'

# Load DB items
DataMapper::setup(:default, ENV['DATABASE_URL'] || "sqlite3://#{Dir.pwd}/database.db")
Dir[libdir + '/models/*.rb'].each { |file| require file }
DataMapper.finalize
DataMapper.auto_upgrade!

# Load routes
Dir[libdir + '/routes/*.rb'].each { |file| require file }

# ...
if Person.all.count == 0
  Person.create! name: "Dani", picture: "http://www.gravatar.com/avatar/2ae6d81e0605177ba9e17b19f54e6b6c.jpg?s=256"
  Person.create! name: "Cristo", picture: "http://www.gravatar.com/avatar/588edc25b4e3459a7e44a4dc719a04d6?s=256"
end

if CurrentIndex.all.count == 0
  CurrentIndex.create! value: 0
end
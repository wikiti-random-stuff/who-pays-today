get '/' do
  @current = Person.all.to_a[CurrentIndex.last.value]
  erb :index
end

post '/next' do
  ci = CurrentIndex.last
  puts ci.value
  ci.update(value: (ci.value + 1) % Person.all.count)
  
  redirect to('/')
end
